import windscribe

windscribe.login('login', 'password')  # Login to windscribe via login password
windscribe.connect(rand=True)  # Connect to random region
windscribe.connect()  # Connect to best (optional region slug)
windscribe.account()  # Get account info
windscribe.disconnect()  # Disconnect from vpn
windscribe.logout()  # Logout from account
